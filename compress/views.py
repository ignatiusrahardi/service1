from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render
from .forms import UploadFileForm
from .models import File
from django.views.decorators.csrf import csrf_exempt
import json
import zipfile
import tarfile
import os
import requests

@csrf_exempt
def compress(request):
    os.system("rm -rf files")
    File.objects.all().delete()
    response = {}
    if request.method == 'POST':
        try:
            checkAuth = ambil_resource(request.headers['Authorization'])
            if checkAuth == 200:
                form = UploadFileForm(request.POST, request.FILES)
                if form.is_valid():
                    # file is saved
                    form.save()
                    files = serializers.serialize('json', File.objects.all())
                    uploads = json.loads(files)
                    url = 'http://' + request.get_host() + '/files/'
                    print(url)
                    for upload in uploads:
                        name = upload['fields']['thefile']
                        compType = upload['fields']['thetype']
                    if compType == 'zip':
                        path = os.path.abspath(os.path.dirname(name)) + '/files/YourCompressedFile.zip'
                        compressing = zipfile.ZipFile(path, 'w')
                        path = os.path.abspath(os.path.dirname(name)) + '/files/' + name
                        compressing.write(path,arcname=name)
                        url += 'YourCompressedFile.zip'
                        response = {
                            'compressed' : url
                        }
                    elif compType == 'tar':
                        path = os.path.abspath(os.path.dirname(name)) + '/files/YourCompressedFile.tar.gz'
                        source_dir = os.path.abspath(os.path.dirname(name)) + '/files/' + name
                        tar = tarfile.open(path, "w:gz") 
                        tar.add(source_dir, arcname=name)
                        tar.close()
                        url += 'YourCompressedFile.tar.gz'
                        
                        response = {
                            'compressed' : url
                        }
                    else:
                        response = {
                            'detail' : 'type of compression must be zip or tar'
                        }
            else:
                response = {
                    'detail' : 'Authentication token expired'
                }
        except KeyError as e:
            response = {
                    'detail' : 'Authentication credentials were not provided.'
            }
    return JsonResponse(response, safe=False)

def ambil_resource(access_token):
    URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
    data_headers = { 'Authorization': 'Bearer ' + access_token }
    r = requests.get(url = URL, headers = data_headers)
    return r.status_code